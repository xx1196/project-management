package co.edu.uniajc.project.management.controller;

import co.edu.uniajc.project.management.model.UserModel;
import co.edu.uniajc.project.management.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@Api("Project")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/")
    @ApiOperation(value = "Users List", response = UserModel.class)
    public List<UserModel> index() {
        return userService.findAll();
    }

    @PostMapping(path = "/")
    @ApiOperation(value = "Create new user", response = UserModel.class)
    public UserModel store(@RequestBody UserModel project) {
        return userService.store(project);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get user by id", response = UserModel.class)
    public Optional<UserModel> show(@PathVariable(value = "id") Long id) {
        return userService.show(id);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update user by id", response = UserModel.class)
    public Optional<UserModel> update(@PathVariable(value = "id") Long id, @RequestBody UserModel project) {
        Optional<UserModel> projectToUpdate = userService.show(id);

        return projectToUpdate.isEmpty() ? projectToUpdate : Optional.ofNullable(userService.update(project));
    }

    // TODO - VALIDAR SI NO ESTÁ EN TAREAS
    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete user by id")
    public void delete(@PathVariable(value = "id") Long id) {
        Optional<UserModel> projectToDelete = userService.show(id);

        if (!projectToDelete.isEmpty()) {
            userService.delete(id);
        }
    }
}
