package co.edu.uniajc.project.management.controller;

import co.edu.uniajc.project.management.model.ProjectModel;
import co.edu.uniajc.project.management.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/project")
@Api("Project")
public class ProjectController {
    private ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping(path = "/")
    @ApiOperation(value = "Project List", response = ProjectModel.class)
    public List<ProjectModel> index() {
        return projectService.findAll();
    }

    @PostMapping(path = "/")
    @ApiOperation(value = "Create new project", response = ProjectModel.class)
    public ProjectModel store(@RequestBody ProjectModel project) {
        return projectService.store(project);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get project by id", response = ProjectModel.class)
    public Optional<ProjectModel> show(@PathVariable(value = "id") Long id) {
        return projectService.show(id);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update project by id", response = ProjectModel.class)
    public Optional<ProjectModel> update(@PathVariable(value = "id") Long id, @RequestBody ProjectModel project) {
        Optional<ProjectModel> projectToUpdate = projectService.show(id);

        return projectToUpdate.isEmpty() ? projectToUpdate : Optional.ofNullable(projectService.update(project));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete project by id")
    public boolean delete(@PathVariable(value = "id") Long id) {
        int count = projectService.countProjectRequirements(id);

        return count == 0 ? projectService.delete(id) : false;
    }
}
