package co.edu.uniajc.project.management.controller;

import co.edu.uniajc.project.management.model.UserModel;
import co.edu.uniajc.project.management.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@Api("Project")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/")
    @ApiOperation(value = "Users List", response = UserModel.class)
    public List<UserModel> index() {
        return userService.findAll();
    }

    @PostMapping(path = "/")
    @ApiOperation(value = "Create new user", response = UserModel.class)
    public UserModel store(@RequestBody UserModel user) {
        return userService.store(user);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get user by id", response = UserModel.class)
    public Optional<UserModel> show(@PathVariable(value = "id") Long id) {
        return userService.show(id);
    }

    @GetMapping(path = "/email/{email}")
    @ApiOperation(value = "Get user by email", response = UserModel.class)
    public Optional<UserModel> getByEmail(@PathVariable(value = "email") String email) {
        return userService.findByEmail(email);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update user by id", response = UserModel.class)
    public Optional<UserModel> update(@PathVariable(value = "id") Long id, @RequestBody UserModel user) {
        Optional<UserModel> userToUpdate = userService.show(id);

        return userToUpdate.isEmpty() ? userToUpdate : Optional.ofNullable(userService.update(user));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete user by id")
    public boolean delete(@PathVariable(value = "id") Long id) {
        int count = userService.countUserTasks(id);

        return count == 0 ? userService.delete(id) : false;
    }
}
