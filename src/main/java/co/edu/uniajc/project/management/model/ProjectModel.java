package co.edu.uniajc.project.management.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Generated
@Entity
@Getter
@Setter
@Table(name = "projects")
public class ProjectModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "client")
    private String client;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "final_date")
    private Date finalDate;

    @Column(name = "status")
    private String status;

    @Column(name = "observations")
    private String observations;
}
