package co.edu.uniajc.project.management.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Generated
@Entity
@Getter
@Setter
@Table(name = "requirements")
public class RequirementModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "id_project")
    private Long idProject;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "final_date")
    private Date finalDate;

    @Column(name = "priority")
    private String priority;

    @Column(name = "status")
    private String status;

    @Column(name = "observations")
    private String observations;
}
