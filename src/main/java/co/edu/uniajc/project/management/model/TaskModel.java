package co.edu.uniajc.project.management.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Generated
@Entity
@Getter
@Setter
@Table(name = "tasks")
public class TaskModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "id_requirement")
    private Long idRequirement;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "final_date")
    private Date finalDate;

    @Column(name = "priority")
    private String priority;

    @Column(name = "status")
    private String status;

    @Column(name = "observations")
    private String observations;

    @Column(name = "id_responsible")
    private Long idResponsible;
}

