package co.edu.uniajc.project.management.repository;

import co.edu.uniajc.project.management.model.ProjectModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProjectRepository extends JpaRepository<ProjectModel, Long> {
    @Query(nativeQuery = true, value = "SELECT count(*) FROM requirements WHERE id_project = :id")
    int countProjectRequirements(@Param(value = "id") Long id);
}
