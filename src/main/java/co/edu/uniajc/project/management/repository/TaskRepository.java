package co.edu.uniajc.project.management.repository;

import co.edu.uniajc.project.management.model.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<TaskModel, Long> {
}
