package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.ProjectModel;
import co.edu.uniajc.project.management.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<ProjectModel> findAll() {
        return projectRepository.findAll();
    }

    public ProjectModel store(ProjectModel project) {
        return projectRepository.save(project);
    }

    public Optional<ProjectModel> show(Long id) {
        return projectRepository.findById(id);
    }

    public ProjectModel update(ProjectModel project) {
        return projectRepository.save(project);
    }

    public boolean delete(Long id) {
        projectRepository.deleteById(id);
        return true;
    }

    public int countProjectRequirements(Long id) {
        return projectRepository.countProjectRequirements(id);
    }
}

