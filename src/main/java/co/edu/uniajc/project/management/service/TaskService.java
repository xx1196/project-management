package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.TaskModel;
import co.edu.uniajc.project.management.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<TaskModel> findAll() {
        return taskRepository.findAll();
    }

    public TaskModel store(TaskModel project) {
        return taskRepository.save(project);
    }

    public Optional<TaskModel> show(Long id) {
        return taskRepository.findById(id);
    }

    public TaskModel update(TaskModel project) {
        return taskRepository.save(project);
    }

    public boolean delete(Long id) {
        taskRepository.deleteById(id);
        return true;
    }
}
