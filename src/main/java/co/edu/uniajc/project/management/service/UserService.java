package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.UserModel;
import co.edu.uniajc.project.management.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserModel> findAll() {
        return userRepository.findAll();
    }

    public UserModel store(UserModel project) {
        return userRepository.save(project);
    }

    public Optional<UserModel> show(Long id) {
        return userRepository.findById(id);
    }

    public Optional<UserModel> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public UserModel update(UserModel user) {
        return userRepository.save(user);
    }

    public boolean delete(Long id) {
        userRepository.deleteById(id);

        return true;
    }

    public int countUserTasks(Long id) {
        return userRepository.countUserTasks(id);
    }
}
