package co.edu.uniajc.project.management;

import co.edu.uniajc.project.management.controller.ProjectController;
import co.edu.uniajc.project.management.model.ProjectModel;
import co.edu.uniajc.project.management.service.ProjectService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class ProjectUnitTest {

    ProjectService projectServiceMock = Mockito.mock(ProjectService.class);

    ProjectController projectController = new ProjectController(projectServiceMock);

    ProjectModel projectModel = new ProjectModel();

    private SimpleDateFormat formato;

    @BeforeEach
    void setUp() {

        formato = new SimpleDateFormat("yyyy-mm-dd");
        projectModel.setId(1L);
        projectModel.setName("Gestion de registros proyectos nuevos");
        projectModel.setClient("Laura sofia");
        try {
            projectModel.setStartDate(formato.parse("2020-09-05"));
            projectModel.setFinalDate(formato.parse("2021-10-31"));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("error try catch");
        }
        projectModel.setStatus("Activo");
        projectModel.setObservations("El proyecto contará con 2 revisiones por mes");
            }

    @Test
    void index() {
        Mockito.when(projectServiceMock.findAll()).thenReturn(Arrays.asList(projectModel));
        projectController.index();
    }

    @Test
    void store() {
        Mockito.when(projectServiceMock.store(any(ProjectModel.class))).thenReturn(projectModel);
        projectController.store(any());
    }

    @Test
    void show() {
        Mockito.when(projectServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(projectModel));
        projectController.show(any());
    }

    @Test
    void update() {
        Mockito.when(projectServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(projectModel));
        Mockito.when(projectServiceMock.update(any(ProjectModel.class))).thenReturn(projectModel);
        projectController.show(any());
        projectController.update(any(), projectModel);
    }

    @Test
    void updateEmpty() {
        Mockito.when(projectServiceMock.show(any())).thenReturn(Optional.empty());
        Mockito.when(projectServiceMock.update(any(ProjectModel.class))).thenReturn(projectModel);
        projectController.show(any());
        projectController.update(any(), projectModel);
    }

    @Test
    void delete() {
        Mockito.when(projectServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(projectModel));
        projectController.delete(any());
    }
}